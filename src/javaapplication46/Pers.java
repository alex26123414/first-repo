
package javaapplication46;

/**
 *
 * @author alex
 */
public class Pers {
    private String name;
    private static int age = 25;

    public Pers() {
    }

    
    public Pers(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getAge() {
        return age;
    }

    public void setAge(int age) {
        Pers.age = age;
    }
    
    @Override
    public String toString() {
        return "Pers{" + "name=" + name + ", age="+age+"}";
    }
    
    
}
